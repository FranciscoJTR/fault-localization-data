#!/usr/bin/env bash
#
# ------------------------------------------------------------------------------
# This script collects all scores for all D4J project/bugs.
# 
# Usage:
# ./collect_scores.sh
#   --input_dir <path>
#   --output_dir <path>
#   [--help]
# 
# Environment variables:
# - D4J_HOME   Needs to be set and must point to the Defects4J installation.
# 
# ------------------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)

#
# Prints an error message to the stderr and exit
#
die() {
  echo "$@" >&2
  exit 1
}

# ------------------------------------------------------------------ Envs & Args

# Check whether D4J_HOME is set
[ "$D4J_HOME" != "" ] || die "D4J_HOME is not set!"
[ -d "$D4J_HOME" ] || die "$D4J_HOME does not exist!"

# Check whether BLACKLIST_FILE exists
BLACKLIST_FILE="$SCRIPT_DIR/../data/blacklist.csv"
[ -s "$BLACKLIST_FILE" ] || die "$BLACKLIST_FILE file does not exist or it is empty!"

USAGE="Usage: ${BASH_SOURCE[0]} --input_dir <path> --output_dir <path> [help]"
if [ "$#" -ne "1" ] && [ "$#" -ne "4" ]; then
  die "$USAGE"
fi

INPUT_DIR=""
OUTPUT_DIR=""

while [[ "$1" = --* ]]; do
  OPTION=$1; shift
  case $OPTION in
    (--input_dir)
      INPUT_DIR=$1;
      shift;;
    (--output_dir)
      OUTPUT_DIR=$1;
      shift;;
    (--help)
      echo "$USAGE"
      exit 0;;
    (*)
      die "$USAGE";;
  esac
done

[ "$INPUT_DIR" != "" ] || die "$INPUT_DIR"
[ -d "$INPUT_DIR" ] || die "$OUTPUT_DIR does not exist"

[ "$OUTPUT_DIR" != "" ] || die "$USAGE"
[ -d "$OUTPUT_DIR" ] || mkdir -p "$OUTPUT_DIR"

# ------------------------------------------------------------------------- Main

SCORES_ARTIFICIAL_VS_REAL_FILE="$OUTPUT_DIR/scores_artificial_vs_real.csv"
>"$SCORES_ARTIFICIAL_VS_REAL_FILE" || die "Cannote write to $SCORES_ARTIFICIAL_VS_REAL_FILE"

SCORES_REAL_EXPLORATION_FILE="$OUTPUT_DIR/scores_real_exploration.csv"
>"$SCORES_REAL_EXPLORATION_FILE" || die "Cannote write to $SCORES_REAL_EXPLORATION_FILE"

for pid in Chart Closure Lang Math Mockito Time; do

  for bid in $(cut -f1 -d',' "$D4J_HOME/framework/projects/$pid/commit-db"); do

    if grep -q "^$pid,.*,$bid," "$BLACKLIST_FILE"; then
      continue
    fi

    if [ "$bid" -gt "1000" ]; then
      MUTANTS_IN_SCOPE="$D4J_HOME/framework/projects/$pid/mutants_in_scope.csv"
      if ! grep -q "^$pid,.*,$bid$" "$MUTANTS_IN_SCOPE"; then
        continue # not in scope
      fi
    fi

    scoring_dir="$INPUT_DIR/$pid/$bid"
    log_file="$scoring_dir/log.txt"
    if [ ! -f "$log_file" ]; then
      continue
    fi
    if ! $(tail -n1 "$log_file" | grep -q "^job has finished successfully\!$"); then
      continue
    fi
    scores_file="$scoring_dir/do-previously-studied-flts/scores.csv"
    [ -s "$scores_file" ] || die "$scores_file does not exist or it is empty!"

    num_entries=$(wc -l "$scores_file" | cut -f1 -d' ')
    [ "$num_entries" -ge "2" ] || die "File $scores_file is incomplete!"

    if [ -s "$SCORES_ARTIFICIAL_VS_REAL_FILE" ]; then
      tail -n +2 "$scores_file" >> "$SCORES_ARTIFICIAL_VS_REAL_FILE"
    else
      cat "$scores_file" > "$SCORES_ARTIFICIAL_VS_REAL_FILE"
    fi

    if [ "$bid" -lt "1000" ]; then
      # `do-full-analysis` is only performed on real faults

      scores_file="$INPUT_DIR/$pid/$bid/do-full-analysis/scores.csv"
      [ -s "$scores_file" ] || die "$scores_file does not exist or it is empty!"

      num_entries=$(wc -l "$scores_file" | cut -f1 -d' ')
      [ "$num_entries" -ge "2" ] || die "File $scores_file is incomplete!"

      if [ -s "$SCORES_REAL_EXPLORATION_FILE" ]; then
        tail -n +2 "$scores_file" >> "$SCORES_REAL_EXPLORATION_FILE"
      else
        cat "$scores_file" > "$SCORES_REAL_EXPLORATION_FILE"
      fi
    fi
  done
done

# Sanity check

if grep -q ",$" "$SCORES_ARTIFICIAL_VS_REAL_FILE"; then
  die "$SCORES_ARTIFICIAL_VS_REAL_FILE is not well formatted!"
fi
if grep -q ",$" "$SCORES_REAL_EXPLORATION_FILE"; then
  die "$SCORES_REAL_EXPLORATION_FILE is not well formatted!"
fi

echo "DONE!"
exit 0
