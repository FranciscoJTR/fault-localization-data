#!/usr/bin/env bash
#
# ------------------------------------------------------------------------------
# This script submits as many jobs (if executed on a cluster) or background
# processes as the number of projects * number of bugs. Each job runs Killmap on
# a specified D4J project/bug using the manually written test cases.
# 
# Usage:
# ./jobs.sh
#   --output_dir <path>
#   [--timeout <hours, 8 by default>]
#   [--partial_output_dir <path, /dev/null by default>]
#   [--help]
# 
# Environment variables:
# - D4J_HOME       Needs to be set and must point to the Defects4J installation.
# 
# ------------------------------------------------------------------------------

SCRIPT_DIR=$(cd `dirname $0` && pwd)
source "$SCRIPT_DIR/../../gzoltar/utils.sh" || exit 1

# ------------------------------------------------------------------ Envs & Args

# Check whether D4J_HOME is set
[ "$D4J_HOME" != "" ] || die "D4J_HOME is not set!"
[ -d "$D4J_HOME" ] || die "$D4J_HOME does not exist!"
export DEFECTS4J_HOME="$D4J_HOME"

# Check whether BLACKLIST_FILE exists
BLACKLIST_FILE="$SCRIPT_DIR/../../data/blacklist.csv"
[ -s "$BLACKLIST_FILE" ] || die "$BLACKLIST_FILE file does not exist or it is empty!"

USAGE="Usage: ${BASH_SOURCE[0]} --output_dir <path> [--timeout <hours, 8 by default>] [--partial_output_dir <path, /dev/null by default>] [help]"
if [ "$#" -ne "1" ] && [ "$#" -gt "6" ]; then
  die "$USAGE"
fi

OUTPUT_DIR=""
TIMEOUT="08"
PARTIAL_OUTPUT_DIR="/dev/null"

while [[ "$1" = --* ]]; do
  OPTION=$1; shift
  case $OPTION in
    (--output_dir)
      OUTPUT_DIR=$1;
      shift;;
    (--timeout)
      TIMEOUT=$1;
      shift;;
    (--partial_output_dir)
      PARTIAL_OUTPUT_DIR=$1;
      shift;;
    (--help)
      echo "$USAGE"
      exit 0;;
    (*)
      die "$USAGE";;
  esac
done

[ "$OUTPUT_DIR" != "" ] || die "$USAGE"
[ -d "$OUTPUT_DIR" ] || mkdir -p "$OUTPUT_DIR"

[ "$TIMEOUT" != "" ] || die "$USAGE"
if _am_I_a_cluster; then
  if [ "$TIMEOUT" -gt "168" ]; then
    die "You have specified a timeout of $TIMEOUT hours. However, the maximum time allowed for jobs to run on the cluster is 168 hours, i.e., 7 days!"
  fi
fi

[ "$PARTIAL_OUTPUT_DIR" != "" ] || die "$USAGE"

# ------------------------------------------------------------------------- Main

for pid in Chart Closure Lang Math Mockito Time; do

  for bid in $(cut -f1 -d',' "$D4J_HOME/framework/projects/$pid/commit-db"); do

    if grep -q "^$pid,.*,$bid," "$BLACKLIST_FILE"; then
      continue
    fi

    if [ "$bid" -gt "1000" ]; then
      MUTANTS_IN_SCOPE="$D4J_HOME/framework/projects/$pid/mutants_in_scope.csv"
      if ! grep -q "^$pid,.*,$bid$" "$MUTANTS_IN_SCOPE"; then
        continue # not in scope
      fi
    fi

    pid_bid_output_dir="$OUTPUT_DIR/killmaps/$pid/$bid"
    rm -rf "$pid_bid_output_dir" && mkdir -p "$pid_bid_output_dir"

    partial_output_file="/dev/null"
    if [ -d "$PARTIAL_OUTPUT_DIR" ]; then
      pid_bid_partial_output_dir="$PARTIAL_OUTPUT_DIR/killmaps/$pid/$bid"

      zip_file="$pid_bid_partial_output_dir/killmap-files.tar.gz"
      if [ -s "$zip_file" ]; then
        log_file_tmp="/tmp/log_file_tmp_$pid-$bid-$$.txt"
        tar -xf "$zip_file" killmaps/$pid/$bid/log.txt -O > "$log_file_tmp"
        if [ $? -ne 0 ]; then
          echo "[ERROR] It was not possible to extract 'log.txt' from '$zip_file', therefore a sanity-check on $pid-$bid could not be performed."
        else
          if grep -q "^DONE\!$" "$log_file_tmp" && ! grep -q " No space left on device$" "$log_file_tmp"; then
            # skip pid-bid if it has completed successfully
            rm -f "$log_file_tmp"
            # to ease the collection of all data, copy existing data to the new
            # output directory (this way, the latest output_dir used has data of
            # *all* pid-bid)
            cp -v "$zip_file" "$pid_bid_output_dir"
            continue
          fi
        fi
        rm -f "$log_file_tmp"

        tar -xf "$zip_file" killmaps/$pid/$bid/killmap.csv.gz -O > "$pid_bid_partial_output_dir/killmap.csv.gz"
        if [ $? -ne 0 ]; then
          echo "[ERROR] It was not possible to extract 'killmap.csv.gz' from '$zip_file'."
        else
          zcat "$pid_bid_partial_output_dir/killmap.csv.gz" > "$pid_bid_partial_output_dir/killmap.csv"
          if [ $? -eq 0 ] && [ -s "$pid_bid_partial_output_dir/killmap.csv" ]; then
            partial_output_file="$pid_bid_partial_output_dir/killmap.csv"
          fi
        fi
      else
        partial_output_file="$PARTIAL_OUTPUT_DIR/killmaps/$pid/$bid/killmap.csv"
        if [ ! -s "$partial_output_file" ]; then
          partial_output_file="/dev/null"
        fi
      fi
    fi

    echo "$pid-$bid"
    log_file="$pid_bid_output_dir/log.txt"
    echo "$pid-$bid" > "$log_file"

    pushd . > /dev/null 2>&1
    cd "$SCRIPT_DIR"
      if _am_I_a_cluster; then
        qsub -V -N "_$bid-$pid" -l h_rt=$TIMEOUT:00:00 -l rmem=8G -e "$log_file" -o "$log_file" -j y \
          "job.sh" --project "$pid" --bug "$bid" --output_dir "$pid_bid_output_dir" --log_file "$log_file" --timeout "$TIMEOUT" --partial_output "$partial_output_file"
      else
        timeout --signal=KILL "${TIMEOUT}h" bash \
          "job.sh" --project "$pid" --bug "$bid" --output_dir "$pid_bid_output_dir" --log_file "$log_file" --timeout "$TIMEOUT" --partial_output "$partial_output_file" > "$log_file" 2>&1 &
        _register_background_process $!
        _can_more_jobs_be_submitted
      fi
    popd > /dev/null 2>&1
  done
done

# It might be the case that there are still jobs running
_wait_for_jobs;

echo "All jobs have been submitted!"
exit 0
